package org.example;

import org.hibernate.Session;

import java.util.Scanner;

public class UserInteraction {
    public static void main(String[] args) throws Exception {
        System.out.println(
                "To add location new location type: 1\n" +
                        "To display added locations type: 2\n" +
                        "To see forecast type: 3\n" +
                        "To finish type: 4"
        );

        Scanner scn = new Scanner(System.in);
        Integer input;

        while (!scn.hasNextInt()) {
            System.out.println("Please enter correct number: ");
            scn = new Scanner(System.in);
        }
        input = scn.nextInt();

        switch (input) {
            case 1:
                Location location = new Location();
                location.setCity("New York");
                location.setLongitude(-74.0059);
                location.setLatitude(40.7128);
                location.setRegion("Northeast");
                location.setCountry("United States");
                Session session = HibernateUtil.getSessionFactory().openSession();
                session.beginTransaction();
                session.persist(location);
                session.getTransaction().commit();
                session.close();

                break;
            case 2:
                System.out.println("2");

                break;
            case 3:
                System.out.println("Enter location:");
                Scanner scn2 = new Scanner(System.in);

                String inputLocation = scn2.nextLine();
                System.out.println(APIConnection.getWeatherData(inputLocation));
                System.out.println("3");
                break;
            case 4:
                System.out.println("4");
                break;
            default:
                System.out.println("Number out of list. Bye");
        }
    }
}

